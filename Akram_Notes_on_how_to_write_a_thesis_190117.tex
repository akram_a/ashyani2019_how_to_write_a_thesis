% The Nordling Lab Presentation example implemented using Beamer by Jose Chang and Torbjörn Nordling.
% Copyright 2018 by Nordling Lab
%
% You are hereby granted the permission to freely copy and modify this file as you see fit, as long as
% you acknowledges Nordling Lab.
%
% NOTE You need to use XeLaTeX to compile this file!

\documentclass{beamer}
\mode<presentation>{
	\usetheme{NordlingLab}
}

%\usepackage[utf8]{inputenc} %XeLaTeX expects UTF8 so not needed
\usepackage[english]{babel}
\let\latinencoding\relax
\usepackage{xltxtra} %Needed to use Candara font
\setsansfont{Candara}
%\usepackage{fontspec} 
%\setmainfont{Cambria} %Alternative font if Candara is not available
\usepackage{graphicx} %Needed to insert images
\graphicspath{./Figures/}
\usepackage[yyyymmdd]{datetime} %Needed to get date on ISO format
\renewcommand{\dateseparator}{--}

\title[Nordling Lab template]{Notes on how to write a thesis}
%\subtitle{Add subtitle here}
\author[T.~Nordling]{Akram Ashyani,~Ph.D.}
\institute[NCKU]{Nordling Lab\\
		Dept. of Mechanical Engineering\\
		National Cheng Kung University\\
		No. 1 University rd., Tainan 70101, Taiwan}
\date{\today}
\subject{Adaptive control}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
\AtBeginSubsection[]{
  \begin{frame}<beamer>{Outline} % frame is only shown in beamer mode
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}


% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command: 
%\beamerdefaultoverlayspecification{<+->}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\setbeamertemplate{background}[NLTitle] %Use this background on title page
\setbeamertemplate{footline}[NLTitle] %Use this empty footline on title page
\begin{frame}
	\titlepage
\end{frame}

\setbeamertemplate{background}[NLCC] %Use this background to get CC license logo
\setbeamertemplate{footline}[NLCC] %Use this footline to get CC license URL
\begin{frame}<beamer>{Outline} % frame is only shown in beamer mode
	\tableofcontents% You might wish to add the option [pausesections]
\end{frame}

% The reminder of this file is with the exception of the slide containing an image covering all of it 
% and the Nordling Lab theme settings identical to the Beamer example conference-ornate-20min.en.tex
% copyrighted by Till Tantau <tantau@users.sourceforge.net>
% Till Tantau has granted the permission to freely copy and modify his file.

% Structuring a talk is a difficult task and the following structure
% may not be suitable. Here are some rules that apply for this
% solution: 

% - Exactly two or three sections (other than the summary).
% - At *most* three subsections per section.
% - Talk about 30s to 2min per frame. So there should be between about
%   15 and 30 frames, all told.

% - A conference audience is likely to know very little of what you
%   are going to talk about. So *simplify*!
% - In a 20min talk, getting the main ideas across is hard
%   enough. Leave out details, even if it means being less precise than
%   you think necessary.
% - If you omit details that are vital to the proof/implementation,
%   just say so once. Everybody will be happy with that.

\section{Template of thesis}
\begin{frame}{Before starting}
\pause
\begin{figure}[h]
\begin{center}
{\resizebox*{10cm}{!}{\includegraphics{Thesis1.jpg}}\hspace{6pt}}
\caption{{
  \small{
\footnotesize
 Writing your thesis. "Piled Higher and Deeper" by Jorge Cham \textcolor{blue}{\href{http://phdcomics.com/comics/archive.php?comicid=1796}{www.phdcomics.com}}
%If you want to have the exact link instead of clickable link in your context use this structure: \textcolor{blue}{\url{http://phdcomics.com/comics/archive.php?comicid=1796}}
}}}
\end{center}
\end{figure}
\end{frame}
\begin{frame}{Thesis structure}
\pause
\textbf{A Thesis is composed of these sections:}
\pause
\begin{enumerate}
\item
\textcolor{red}{Abstract}\\ \pause
\item Introduction/Literature review\\ \pause
\item Materials and methods\\ \pause
\item Results and discussion\\ \pause
\item \textcolor{red}{Conclusion}\\ \pause
\item Suggestions for future works
\end{enumerate}
\end{frame}

\begin{frame}{How to write the structured “abstract”?}
\pause
\textbf{To write a \textcolor{blue}{\href{https://bitbucket.org/temn/nordlinglab-web/wiki/how_to_write_good_abstract_for_research\%20project_proposal}{structured abstract}}:}

 \pause
Firstly: finish your thesis (or your paper) 

 \pause
Secondly: type a summary that identifies
\\ \pause
\begin{enumerate}
\item
Your problem/question
\\ \pause
\item
 The reason of being interested on this problem
      \\ \pause
\item
 Previous works
      \\ \pause
\item
Expected results
      \\ \pause
\item 
 The expected implications and conclusions     
\end{enumerate}
\end{frame}
\begin{frame}{How to write the structured “abstract”?}
       \pause
Thirdly, format it as below:

 \pause
 \begin{enumerate}
 \item Motivation
      \\ \pause
 \item  Background
      \\ \pause
  \item     Problem definition
      \\ \pause
  \item     Expected results
      \\ \pause
  \item     Expected implications
 \end{enumerate}
\end{frame}

\begin{frame}{How to write the “introduction”?}
\pause
\textbf{For writing the introduction:}
\pause
\begin{enumerate}
\item
Establish topic
 \\ \pause
\item
Provide significance
 \\ \pause
\item
Review the relevant literature
 \\ \pause
\item
\textcolor{red}{Point out the gap}
 \\ \pause
\item
Reveal the research question (and sometimes, hypotheses)
\end{enumerate}
\end{frame}

\begin{frame}{How to write the “materials and methods”?}
\pause
\textbf{In materials and methods, you should provide information about:}
\pause
\begin{enumerate}
\item Methods of sample preparation
\\ \pause
\item Dependent and independent parameters
\\ \pause
\item Equipment and their companies and models
\\ \pause
\item Any software or programming language and their versions
\\ \pause
\item Hypotheses and assumptions of the work, boundary conditions, …
\end{enumerate}
\end{frame}

\begin{frame}{How to write the “results and discussion”?}
\pause
\textbf{Results and discussion can be written in one part or in two separate parts. }
\pause
\begin{enumerate}
\item[\bullet] Results:
\\ \pause
\begin{enumerate}
\item The obtained results should be mentioned in this part. 
 \\ \pause
\item     Graphs and curves are encouraged more than tables and text.
\pause
\end{enumerate}
\item[\bullet] Discussion:
\\ \pause
\begin{enumerate}
\item  Reasons and theories behind the obtained results have to be investigated.
 \\ \pause
\item     Results should be compared with other works to verify or to discuss the reasons for differences.
\end{enumerate}
\end{enumerate} 
         
\end{frame}

\begin{frame}{How to write the “conclusion”?}
\pause
\begin{enumerate}
\item[\bullet]
All conclusion answers the \textit{primary research question.} \pause
\item[\bullet]
A brief summary, just a few paragraphs, of your \textit{key findings.} \pause
\item[\bullet]
The \textit{conclusions} which you have drawn from your research. \pause
\item[\bullet]
A final paragraph \textit{rounding off} your paper or thesis. \pause
\item[\bullet]
Future works which includes \textit{suggestions} for the continuation of the research or filling other gaps.
\end{enumerate}
\end{frame}

\begin{frame}{Other things…}
\pause
\begin{enumerate}
\item[\bullet] Table of contents \\ 
     It should include all headings and subheadings.\pause
\item[\bullet] Table of figures  \\ 
      It should include all figure captions .\pause
\item[\bullet] Table of tables  \\ 
It should include all table captions . \pause
\item[\bullet] References  \\ 
   All references mentioned in the reference list are cited in the text, and vice versa.\pause
\item[\bullet] Appendices  \\ 
 General and relevant information in forms of tables, standards, …
\end{enumerate}     
\end{frame}
\begin{frame}{General comments}
\pause
\begin{enumerate}
\item Don't use so short sentences.
\pause
\item Avoid using long sentences(Approximately over thirty two words).
\pause
\item Mix!
\pause
\item Take care about using conjunction and connecting words.
\pause
\item Notice on your tenses.
\pause \item Do not use abbreviation and long sentences on the abstract.
\end{enumerate}
\end{frame}
\begin{frame}{Finally…}
\pause
\textit{\textbf{Check, Check and Check Again.}}
\end{frame}
\section{What I have done and what you are expected to do}
\subsection{What I have done}
\begin{frame}{What I have done}
\pause
\begin{enumerate}
\item 
A comprehensive search in the library and in the internet for finding some good resources for you.
\item
Finding good samples for thesis.
\item
Finding the best way of being update for your research.
\end{enumerate}
\end{frame}
\subsection{What you are expected to do}
\begin{frame}{What you are expected to do}
\pause
\textbf{Studying these theses with concentration on  their structures:}
\pause
\begin{enumerate}
\item 
Lin2009, A Shape Memory Alloy Actuated Microgripper with Wide Handling Ranges, MScThesis, NCKU 
\item 
Liou2009, Computational Models for Analyzing Compliant Mechanisms Software Development with Experiments, MScThesis, NCKU
\item 
Yang2008, Design of a Compliant Robotic Hand Actuated by Shape Memory Alloy Wires, MScThesis, NCKU
\end{enumerate}
\end{frame}
\begin{frame}{What you are expected to do}
\pause
\begin{enumerate}
\item 
Sari2014, Synthesis of Carbon Nano-Materials in Methane-Ethylene Jet Diffusion Flames Modulated by Acoustic Excitation, MScThesis, NCKU
\item 
Selva2013, Oxy-oil Combustion at Positive Pressure in a Retroffited 300kW Furnace, MScThesis, NCKU
\item 
Syawitri, Numerical Investigation into The Effect of Metal-foam Volume Fraction on The Performance of Metal Hydride Reactor Subjected to Periodic Charging and Discharging, MScThesis, NCKU
\end{enumerate}
\end{frame}

\begin{frame}{What you are expected to do}
\pause
\begin{enumerate}
\item
Identify 5 key articles and key words of your own projects 
\item 
Subscribe articles in the Scopus alert.
\item Subscribe key words to alerts of  Google Scholar.
\end{enumerate}
\pause

These links might be helpful in this case:
\pause
\begin{enumerate}
\item
\textcolor{blue}{\href{https://hub.wiley.com/community/exchanges/discover/blog/2015/04/02/how-can-i-use-google-scholar-citations-for-scholarly-profiling}{Use Google Scholar citations for scholarly profiling}}
\item
\textcolor{blue}{\href{https://www.youtube.com/watch?v=ZWBgh50iax8}{Use Google Scholar citation}}
\item
\textcolor{blue}{\href{https://tutorials.scopus.com/EN/Personalization/sc_Personal_textOnly.html}{Creating alert at the Scopus}}
\end{enumerate}
\end{frame}



\begin{frame}{For Further Reading}
\pause
Firstly, read the writing, Master and PhD program sections on the \textcolor{blue}{\href{https://bitbucket.org/temn/nordlinglab-web/wiki/Home#markdown-header-master-program}{Wiki page}}.
\\ \pause
Secondly, use below links:
\begin{enumerate}
\item
\textcolor{blue}{\href{https://bitbucket.org/temn/nordlinglab-web/wiki/howtowriteanarticle}{How to write an article}}
\item
\textcolor{blue}{\href{http://www.nature.com/authors/author_resources/how_write.html}{Writing for a nature journal}}
\item
\textcolor{blue}{\href{https://www.dropbox.com/s/1slrzsioz74si46/Sage2003_Writing_a_clear_and_engaging_paper.doc?dl=0}{Writing clear and engaging paper}}
\item
\textcolor{blue}{\href{https://www.dropbox.com/s/gz1q3y6t28x11os/Hartley2014_Current_findings_structured_abstracts.pdf?dl=0}{Structured abstract}}
\item
\textcolor{blue}{\href{https://www.dcu.ie/sites/default/files/students_learning/docs/WC_Numbers-in-academic-writing.pdf}{Numbers in academic writing}}
\item
\textcolor{blue}{\href{https://pingpong.chalmers.se/public/pp/public_courses/course08583/published/1510227352918/resourceId/4156227/content/Zobel\%20-\%20Writing\%20for\%20computer\%20science\%203rd\%20edition.pdf}{Writing for Computer Science}}
\item
\textcolor{blue}{\href{http://weblis.lib.ncku.edu.tw/search~S1*eng?/XAcademic+Writing+and+Publishing+&searchscope=1&SORT=D/XAcademic+Writing+and+Publishing+&searchscope=1&SORT=D&SUBKEY=Academic+Writing+and+Publishing+/1\%2C80\%2C80\%2CB/frameset&FF=XAcademic+Writing+and+Publishing+&searchscope=1&SORT=D&1\%2C1\%2C}{Academic writing and publishing}}


\end{enumerate}
\end{frame}
\begin{frame}{Thank you for your consideration and good luck}
\begin{figure}[h]
\begin{center}
{\resizebox*{8.9cm}{!}{\includegraphics{Thesis2.jpg}}\hspace{6pt}}
\caption{{
  \small{
\footnotesize
 How to write your thesis in ten minutes a day. "Piled Higher and Deeper" by Jorge Cham \textcolor{blue}{\href{http://phdcomics.com/comics/archive.php?comicid=1597}{www.phdcomics.com}}
}}}
\end{center}
\end{figure}
\end{frame}

% All of the following is optional and typically not needed. 
%\appendix
%\section<presentation>*{\appendixname}
%\subsection<presentation>*{For Further Reading}

%\begin{frame}[allowframebreaks]
%  \frametitle<presentation>{For Further Reading}
    
 % \begin{thebibliography}{10}
    
%  \beamertemplatebookbibitems
  % Start with overview books.

%  \bibitem{Author1990}
%    A.~Author.
%    \newblock {\em Handbook of Everything}.
%    \newblock Some Press, 1990.
 
    
%  \beamertemplatearticlebibitems
  % Followed by interesting articles. Keep the list short. 

%  \bibitem{Someone2000}
%    S.~Someone.
%    \newblock On this and that.
 %   \newblock {\em Journal of This and That}, 2(1):50--100,
%    2000.
 % \end{thebibliography}
%\end{frame}

\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


